<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UserTableSeeder extends Seeder
{
    /**
        The migrate:fresh command will drop all tables from the database and then execute the  migrate command:
        php artisan migrate:fresh
        php artisan migrate:fresh --seed
     **/
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Retrieve the first model matching the query constraints... (first())
        $role_user = Role::where('name','User')->first();
        $role_author = Role::where('name','Author')->first();
        $role_admin = Role::where('name','Admin')->first();

        //Bcrypt hashing for storing user passwords.
        $user = new User();
        $user->name = 'User';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('user');
        $user->save();
        $user->roles()->attach($role_user);

        $author = new User();
        $author->name = 'Author';
        $author->email = 'author@gmail.com';
        $author->password = bcrypt('author');
        $author->save();
        $author->roles()->attach($role_author);

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
