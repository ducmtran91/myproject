<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'User';
        $role_user->description = 'A Normal User';
        $role_user->save();

        $role_user = new Role();
        $role_user->name = 'Author';
        $role_user->description = 'An Author';
        $role_user->save();

        $role_user = new Role();
        $role_user->name = 'Admin';
        $role_user->description = 'A Admin';
        $role_user->save();
    }
}
